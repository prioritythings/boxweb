VAGRANTFILE_API_VERSION = "2"
require 'json'
require 'yaml'
$root_path = File.dirname(__FILE__)
$config_path = File.join($root_path, 'config', 'config.yml')
$repositories_path = File.join($root_path, 'config', 'repositories.json')
$ports_path = File.join($root_path, 'config', 'ports.json')

if File.exists?($config_path) && File.exists?($repositories_path)
  $cfg = YAML.load_file($config_path)
  VM_IP = $cfg[:vm_ip]
  VM_NAME = $cfg[:vm_name]
  VM_HOSTNAME = $cfg[:vm_hostname]
  VM_ALIASES = $cfg[:vm_aliases]
  VM_MEMORY = $cfg[:vm_memory]
  VM_BOX = $cfg[:vm_box]
  ENABLE_PASSWORD_LOGIN = $cfg[:enable_password_login]
  SSH_USERNAME = $cfg[:ssh_username]
  SSH_PASSWORD = $cfg[:ssh_password]
  # repo
  repository_file = File.read($repositories_path)
  $repo_json_data = JSON.parse(repository_file)
  repositories = Array.new 
  $repo_json_data.each do |ele|
    repositories.push({ "src": ele['src'], "dest": ele['dest'] })
  end
  # ports
  port_file = File.read($ports_path)
  $port_json_data = JSON.parse(port_file)
  forwarded_ports = Array.new 
  $port_json_data.each do |port|
    forwarded_ports.push({ "guest": port['guest'], "host": port['host'] })
  end
  
  required_plugins = %w(vagrant-hostsupdater vagrant-bindfs)
  plugins_to_install = required_plugins.select { |plugin| not Vagrant.has_plugin? plugin }
  if not plugins_to_install.empty?
    system "vagrant plugin install #{plugins_to_install.join(' ')}"
    exec "vagrant #{ARGV.join(' ')}"
  end

  Vagrant.configure(VAGRANTFILE_API_VERSION) do |cf|
    cf.vm.define VM_NAME do |config|
      # box
      config.vm.box = VM_BOX
      config.vm.box_check_update = false
      config.vm.box_download_insecure = true
      if ENABLE_PASSWORD_LOGIN
        config.ssh.username = SSH_USERNAME
        config.ssh.password = SSH_PASSWORD
      end
      #network
      for forwarded_port in forwarded_ports
        config.vm.network "forwarded_port", guest: forwarded_port[:guest], host: forwarded_port[:host]        
      end      
      config.vm.network :private_network, ip: VM_IP

      #host
      config.vm.hostname = VM_HOSTNAME
      config.hostsupdater.aliases = VM_ALIASES

      #provider
      config.vm.provider "virtualbox" do |vb|
        vb.name = VM_HOSTNAME
        vb.customize ["modifyvm", :id, "--cpuexecutioncap", "50"]
        vb.customize ["modifyvm", :id, "--hwvirtex", "on"]
        vb.customize ["modifyvm", :id, "--cpus", "1"]
        vb.customize ["modifyvm", :id, "--memory", VM_MEMORY]
        vb.customize ["modifyvm", :id, "--uartmode1", "disconnected" ]
      end

      # sync configs, webs dir
      config.vm.synced_folder ".", "/vagrant", disabled: true
      config.vm.synced_folder "./shared/", "/home/ubuntu/shared", create: true, disabled: false,
        owner: "ubuntu", group: "ubuntu"
      # sync source, ignore .git
      for repository in repositories
        config.vm.synced_folder repository[:src] , repository[:dest], create: true, disabled: false,
                              owner: "ubuntu", group: "ubuntu"
      end
      # provision
      config.vm.provision "shell" do |s|
        s.inline = "echo welcome, " + VM_HOSTNAME
      end
      config.vm.provision "shell", path: "provision.sh"

      # success message
      config.vm.post_up_message = VM_HOSTNAME + " is ready now"
    end
  end
else
  puts "Config files were not found!"
  exit(0)
end