# Cloud002 Info

- Cloud name : boxweb.local
- Users: ubuntu | 12345678 , vagrant | ssh
- OS : Ubuntu 16.0.4 LTS

```
vagrant up --provider=virtualbox
```

## Change user password

```bash
sudo su
passwd ubuntu
123456
exit
vagrant reload
```

## Change login method from ssh key to password auth

```json
:enable_password_login: true
:ssh_username: "ubuntu"
:ssh_password: "123456"
```

```bash
nano /etc/ssh/sshd_config
# PasswordAuthentication yes
# PermitRootLogin yes
sudo service ssh restart
sudo systemctl status sshd
```


# Install common stacks

- [] DB:Redis

